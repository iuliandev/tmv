<?php

namespace App\Listeners;

use App\Events\NewCommentReplyEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewCommentReplyPushListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewCommentReplyEvent  $event
     * @return void
     */
    public function handle(NewCommentReplyEvent $event)
    {
        $title = 'Reply to your comment';
        $body = $event->user->firstname . ' ' . $event->user->lastname . ' replied to your comment.';

        pushNotification($event->to_user, $title, $body);
    }
}

<?php

namespace App\Listeners;

use App\Events\NewCommentLikeEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewCommentLikePushListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewCommentLikeEvent  $event
     * @return void
     */
    public function handle(NewCommentLikeEvent $event)
    {
        $title = 'New reaction to comment';
        $body = $event->user->firstname . ' ' . $event->user->lastname . ' liked your comment.';

        pushNotification($event->to_user, $title, $body);
    }
}

<?php

namespace App\Listeners;

use App\Events\NewCommentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewCommentPushListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewCommentEvent  $event
     * @return void
     */
    public function handle(NewCommentEvent $event)
    {
        $title = 'New comment';
        $body = $event->user->firstname . ' ' . $event->user->lastname . ' commented to your post.';

        pushNotification($event->to_user, $title, $body);
    }
}

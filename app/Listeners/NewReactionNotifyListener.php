<?php

namespace App\Listeners;

use App\Events\NewReactionEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\NewReaction;

class NewReactionNotifyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewReactionEvent  $event
     * @return void
     */
    public function handle(NewReactionEvent $event)
    {
        $event->to_user->notify(new NewReaction($event->user));
    }
}

<?php

namespace App\Listeners;

use App\Events\NewReactionEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewReactionPushListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewReactionEvent  $event
     * @return void
     */
    public function handle(NewReactionEvent $event)
    {
        $title = 'New reaction to post';
        $body = $event->user->firstname . ' ' . $event->user->lastname . ' reacted to your post.';

        pushNotification($event->to_user, $title, $body);
    }
}

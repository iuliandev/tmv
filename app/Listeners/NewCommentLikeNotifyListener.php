<?php

namespace App\Listeners;

use App\Events\NewCommentLikeEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\NewCommentLike;

class NewCommentLikeNotifyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewCommentLikeEvent  $event
     * @return void
     */
    public function handle(NewCommentLikeEvent $event)
    {
        $event->to_user->notify(new NewCommentLike($event->user));
    }
}

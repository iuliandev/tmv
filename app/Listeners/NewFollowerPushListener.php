<?php

namespace App\Listeners;

use App\Events\NewFollowerEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewFollowerPushListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewFollowerEvent  $event
     * @return void
     */
    public function handle(NewFollowerEvent $event)
    {
        $title = 'New connection';
        $body = $event->user->firstname . ' ' . $event->user->lastname . ' started follow you.';

        pushNotification($event->to_user, $title, $body);
    }
}

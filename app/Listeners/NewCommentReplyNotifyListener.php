<?php

namespace App\Listeners;

use App\Events\NewCommentReplyEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\NewReplyToComment;

class NewCommentReplyNotifyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewCommentReplyEvent  $event
     * @return void
     */
    public function handle(NewCommentReplyEvent $event)
    {
        $event->to_user->notify(new NewReplyToComment($event->user));
    }
}

<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\User;

class NewReactionEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    public $to_user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $to_user)
    {
        $this->user = auth()->user();
        $this->to_user = $to_user;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MomStatus
 *
 * @property int $id
 * @property string $title
 *
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class MomStatus extends Model
{
		protected $table = 'mom_status';
		public $timestamps = false;

		protected $fillable = [
				'title'
		];

		public function users()
		{
				return $this->hasMany('\App\User');
		}

		/**
     * Get Mom Status Options
     *
     * @return $this
     */
		public function getOptions()
		{
				return $this->get()->pluck('title', 'id');
		}
}

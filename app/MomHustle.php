<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MomHustle
 *
 * @property int $id
 * @property string $title
 *
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class MomHustle extends Model
{
		protected $table = 'mom_hustle';
		public $timestamps = false;

		protected $fillable = [
				'title'
		];

		public function users()
		{
				return $this->hasMany('\App\User');
		}

		/**
		 * Get Mom Hustle Options
		 *
		 * @return $this
		 */
		public function getOptions()
		{
				return $this->get()->pluck('title', 'id');
		}
}

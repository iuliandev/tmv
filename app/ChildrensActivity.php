<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChildrensActivity
 *
 * @property int $id
 * @property string $title
 *
 * @package App\Models
 */
class ChildrensActivity extends Model
{
		public $timestamps = false;

		protected $fillable = [
				'title'
		];

		public function user()
		{
				return $this->belongsToMany('\App\User', 'user_childrens_activities', 'childrens_activities_id', 'user_id');
		}

		/**
		 * Get Children Activities Options
		 *
		 * @return $this
		 */
		public function getOptions()
		{
				return $this->get()->pluck('title', 'id');
		}
}

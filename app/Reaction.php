<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id',
        'user_id',
        'reaction'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function post()
    {
        return $this->belongsToMany('App\Post');
        // return $this->belongsToMany('App\Post', 'reactions', 'post_id', 'user_id')
        //             ->withPivot(["reaction"]);
    }

    /**
     * Find post reaction
     *
     * @param Post $post
     *
     * @return $this
     */
    public function findReaction($post)
    {
        $reaction = $post->reactions()
                         ->where('user_id', auth()->user()->id)
                         ->first();

        return $reaction;
    }

    /**
     * Add post reaction
     *
     * @param Post $post
     *
     * @param string $type
     */
    public function addReaction($post, $type)
    {
        $post->reactions()->create(array(
            'user_id' => auth()->user()->id,
            'reaction' => $type
        ));
    }

    /**
     * Update post reaction or remove if exist
     *
     * @param Reaction $reaction
     *
     * @param string $type
     */
     public function updateOrDetachReaction($reaction, $type)
     {
         if($reaction->reaction == $type) {
             $reaction->delete();
         } else {
             $reaction->reaction = $type;
             $reaction->save();
         }
     }
}

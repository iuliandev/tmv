<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MatriarchLife
 *
 * @property int $id
 * @property string $title
 *
 * @package App\Models
 */
class MatriarchLife extends Model
{
		protected $table = 'matriarch_life';
		public $timestamps = false;

		protected $fillable = [
				'title'
		];

		public function user()
		{
				return $this->belongsToMany('\App\User', 'user_matriarch_life', 'matriarch_life_id', 'user_id');
		}

		/**
		 * Get Matriarch Life Options
		 *
		 * @return $this
		 */
		public function getOptions()
		{
				return $this->get()->pluck('title', 'id');
		}
}

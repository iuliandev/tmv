<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'post_id',
        'parent_id',
        'comment'
    ];

    /**
     * The data type attributes
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'updated_at',
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function replies()
    {
        return $this->hasMany('App\Comment', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Comment');
    }

    public function likes()
    {
        return $this->belongsToMany('App\User', 'comment_likes', 'comment_id', 'user_id')
                    ->withTimestamps();
    }

    /**
     * Find comment
     *
     * @param int $comment_id
     *
     * @return $this
     */
    public function findMyComment($comment_id)
    {
        $comment = $this->where('user_id', auth()->user()->id)
                        ->where('id', $comment_id)
                        ->first();

        return $comment;
    }
}

<?php

namespace App;

use App\Notifications\NewFollower;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
     		'firstname',
     		'lastname',
     		'email',
     		'age',
     		'zip_code',
     		'position',
     		'mom_status_id',
     		'mom_hustle_id',
     		'short_bio',
     		'avatar',
        'app_token',
     		'email_verified_at',
     		'password',
     		'remember_token'
   	];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'app_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The data type attributes
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'updated_at',
        'created_at'
    ];

    public function mom_hustle()
    {
        return $this->belongsTo('\App\MomHustle');
    }

    public function mom_status()
    {
        return $this->belongsTo('\App\MomStatus');
    }

    public function matriarch_life()
    {
        return $this->belongsToMany('\App\MatriarchLife', 'user_matriarch_life', 'user_id', 'matriarch_life_id');
    }

    public function childcare_management()
    {
        return $this->belongsToMany('\App\ChildcareManagement', 'user_childcare_management', 'user_id', 'childcare_management_id');
    }

    public function childrens_activities()
    {
        return $this->belongsToMany('\App\ChildrensActivity', 'user_childrens_activities', 'user_id', 'childrens_activities_id');
    }

    public function momming_hard()
    {
        return $this->hasMany('App\MommingHard');
    }

    public function AauthAcessToken() {
        return $this->hasMany('\App\OauthAccessToken');
    }

    public function posts() {
        return $this->hasMany('App\Post');
    }

    public function in_posts()
    {
        return $this->belongsToMany('App\Post', 'post_with_users', 'user_id', 'post_id');
    }

    public function reacted()
    {
        return $this->hasMany('App\Reaction');
    }

    public function followers()
    {
        return $this->belongsToMany('App\User', 'followers', 'leader_id', 'follower_id')
                    ->withTimestamps();
    }

    public function followings()
    {
        return $this->belongsToMany('App\User', 'followers', 'follower_id', 'leader_id')
                    ->withTimestamps();
    }

    /**
     * Set email
     *
     * @param string $email mail
     *
     * @return $this
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password Password
     *
     * @return $this
     */
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set firstname
     *
     * @param string $firstname FirstName
     *
     * @return $this
     */
    public function setFirstname(string $firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Set lastname
     *
     * @param string $lastname LastName
     *
     * @return $this
     */
    public function setLastname(string $lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Set age
     *
     * @param string $age Age
     *
     * @return $this
     */
    public function setAge(int $age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Set Zip code
     *
     * @param string $zip_code Zip code
     *
     * @return $this
     */
    public function setZipCode(string $zip_code)
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    /**
     * Set Position code
     *
     * @param string $position Position
     *
     * @return $this
     */
    public function setPosition(string $position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Set app token
     *
     * @param string $app_token
     *
     * @return $this
     */
    public function setAppToken(string $app_token = null)
    {
        $this->app_token = $app_token;

        return $this;
    }

    /**
     * Set Default avatar
     *
     * @return $this
     */
    public function setDefaultAvatar()
    {
        $this->avatar = 'default/avatar.png';

        return $this;
    }

    /**
     * Set Mom status
     *
     * @param int $mom_status Mom status
     *
     * @return $this
     */
    public function setMomStatus(int $mom_status_id)
    {
        $this->mom_status_id = $mom_status_id;

        return $this;
    }

    /**
     * Set Mom hustle
     *
     * @param int $mom_hustle Mom hustle
     *
     * @return $this
     */
    public function setMomHustle(int $mom_hustle_id)
    {
        $this->mom_hustle_id = $mom_hustle_id;

        return $this;
    }

    /**
     * Set Matriarch Life
     *
     * @param array $matriarch_life Matriarch Life
     *
     * @return $this
     */
    public function setMatriarchLife(array $matriarch_life)
    {
        $this->matriarch_life()->sync($matriarch_life);

        return $this;
    }


    /**
     * Set Childcare Management
     *
     * @param array $childcare_management Childcare Management
     *
     * @return $this
     */
    public function setChildcareManagement(array $childcare_management)
    {
        $this->childcare_management()->sync($childcare_management);

        return $this;
    }

    /**
     * Set Childrens Activities
     *
     * @param array $childrens_activities Childrens Activities
     *
     * @return $this
     */
    public function setChildrensActivities(array $childrens_activities)
    {
        $this->childrens_activities()->sync($childrens_activities);

        return $this;
    }

    /**
     * Set Momming Hard
     *
     * @param array $momming_hard Momming Hard
     *
     * @return $this
     */
    public function setMommingHard(array $momming_hard)
    {
        if(count($momming_hard) > 0) {
            foreach ($momming_hard as $gender => $dates) {
                foreach ($dates as $nth_child => $date) {
                   $this->momming_hard()->create([
                        'nth_child'  => $nth_child,
                        'birth_date' => $date,
                        'gender'     => $gender
                    ]);
                }
            }
        }

        return $this;
    }

    /**
     * Search users
     *
     * @param string $name name of User
     *
     * @return $this
     */
    public function searchUsers($name)
    {
        $users = $this->where(function($q) use ($name) {
                    $q->where(\DB::raw("CONCAT(`firstname`, ' ', `lastname`)"), 'LIKE', "%".$name."%")
                      ->orWhere('firstname', 'LIKE', "%".$name."%");
        });

        return $users;
    }
}

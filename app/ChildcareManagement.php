<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChildcareManagement
 *
 * @property int $id
 * @property string $title
 * @property string $type
 *
 * @package App\Models
 */
class ChildcareManagement extends Model
{
		protected $table = 'childcare_management';
		public $timestamps = false;

		protected $fillable = [
				'title',
				'type'
		];


		public function user()
		{
				return $this->belongsToMany('\App\User', 'user_childcare_management', 'childcare_management_id', 'user_id');
		}

		/**
		 * Get Childcare Management Options
		 *
		 * @return $this
		 */
		public function getOptions()
		{
				$result = $this->get()->groupBy('type');

				$result = $result->map(function ($item) {
				    return $item->pluck('title', 'id');
				});

				return $result;
		}
}

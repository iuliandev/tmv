<?php

use GuzzleHttp\Client;

function pushNotification($to_user, $title, $body = null) {
    if($to_user->app_token) {
        $client  = new Client();

        $message = [
            'to' => $to_user->app_token,
            'notification' => [
                'title'              => $title,
                'body'               => $body,
                'content_available'  => true,
                'priority'           => 'high'
            ]
        ];

        $client = new GuzzleHttp\Client([
            'headers' => [
                'Content-Type'  => 'application/json',
                'Authorization' => env('FIREBASE_AUTH_KEY')
            ]
        ]);

        $response = $client->post('https://fcm.googleapis.com/fcm/send',
            ['body' => json_encode($message)]
        );

        return $response;
    }
}

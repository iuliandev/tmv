<?php

function mediaFile($entity, $file)
{
    if($file) {
        return url('/') . '/media/' . $entity . '/' . $file;
    }

    return null;
}

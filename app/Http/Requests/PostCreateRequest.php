<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'nullable|string|max:255',
            'text'  => 'nullable|string|max:255',
            'media' => 'nullable|mimes:jpeg,png,jpg,gif,mp3,webm,mpga,wav,wma,ogg,mpc,mogg,mpeg,mp4,m4v,3gp,mov,qt|max:8192'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'media.mimes' => 'Please try to choose a valid image, video or audio file.'
        ];
    }
}

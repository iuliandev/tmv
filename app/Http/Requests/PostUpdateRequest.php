<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'nullable|string|max:255',
            'text'  => 'nullable|string|max:255',
            // 'media' => 'nullable|mimes:jpeg,png,jpg,gif,mp3,webm,mpga,wav,wma,ogg,mpc,mogg,mpeg,mp4,3gp|max:8192'
        ];
    }
}

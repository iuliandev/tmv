<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRegisterStep2Request extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'    => 'required|string',
            'lastname'     => 'required|string',
            'email'        => 'required|email|unique:users,email,'.auth()->user()->id,
            'age'          => 'required|integer',
            'zip_code'     => 'required|string',
            'position'     => 'nullable|string|max:255',
        ];
    }
}

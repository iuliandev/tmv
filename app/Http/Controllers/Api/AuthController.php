<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Auth\AuthRepositoryInterface;
use App\Http\Controllers\Api\AppController;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\User as UserResource;
use App\Http\Requests\UserRegisterStep1Request;
use App\Http\Requests\UserRegisterStep2Request;
use App\Http\Requests\UserLoginRequest;
use App\User;
use Auth;

class AuthController extends AppController
{
    /**
    * User repository
    */
    private $userRepository;

    /**
    * Auth repository
    */
    private $authRepository;

    /**
    * Constructor
    *
    * @param UserRepositoryInterface $userRepository User repository
    * @param AuthRepositoryInterface $authRepository Auth repository
    */
    public function __construct(
        UserRepositoryInterface $userRepository,
        AuthRepositoryInterface $authRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->authRepository = $authRepository;
    }

    /**
     * Create new user step 1
     *
     * @param UserRegisterStep1Request $request
     *
     * @return array
     */
    public function register(UserRegisterStep1Request $request) {
        try {
            $user = new User;
            $response = array();

            $this->authRepository
                 ->populateFirstStep($user, $request);

            $this->authRepository
                 ->setTokenExpiration();

            $user = Auth::loginUsingId($user->id);
            $response['user'] = new UserResource($user);
            $response['access_token'] = $this->authRepository
                                             ->getToken($user);

            return jsonResponse('success', 201, $response);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Create new user step 2
     *
     * @param UserRegisterStep2Request $request
     *
     * @return array
     */
    public function registerContinue(UserRegisterStep2Request $request) {
        try {
            $this->authRepository
                 ->populateSecondStep($request);

            $user = $this->userRepository
                         ->getAuthUser();

            return jsonResponse('success', 200, new UserResource($user));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Login
     *
     * @param UserLoginRequest $request
     *
     * @return array
     */
    public function login(UserLoginRequest $request) {
        try {
            $user = $this->authRepository
                         ->getUserByEmail($request->email);

            if(!$user)
                return jsonResponse('error', 401, [
                    'email' => 'Account with this email does not exist.'
                ]);

            if(Hash::check($request->password, $user->password)) {
                $this->authRepository
                     ->setTokenExpiration();

                $user = Auth::loginUsingId($user->id);
                $this->userRepository->updateAppToken($request->app_token);
                $response['user'] = new UserResource($user);
                $response['access_token'] = $this->authRepository
                                                 ->getToken($user);

                return jsonResponse('success', 200, $response);
            }

            return jsonResponse('error', 401, [
                'password' => 'Your password is incorrect.'
            ]);

        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Log out
     *
     * @return array
     */
    public function logout()
    {
        try {
            if (auth()->check()) {
                $this->authRepository->deleteToken();
                $this->userRepository->updateAppToken(null);

                return jsonResponse('success', 200, [
                    'message' => 'You was logout.'
                ]);
            }

            return jsonResponse('error', 400, [
                'message' => 'You are loggout.'
            ]);
        } catch(\Ecception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Registration inputs
     *
     * @return array
     */
    public function inputs() {
        try {
            $inputs = $this->authRepository->getInputs();

            return jsonResponse('success', 200, $inputs);
        } catch(\Ecception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AppController;
use App\Repositories\Feed\FeedRepositoryInterface;
use App\Http\Resources\PostCollection;

class FeedController extends AppController
{
    /**
     * Feed repository
     *
     * @var FeedRepositoryInterface
     */
    private $feedRepository;

    /**
     * Constructor
     *
     * @param FeedRepositoryInterface $feedRepository Feed repository
     */
    public function __construct(FeedRepositoryInterface $feedRepository)
    {
        $this->feedRepository = $feedRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(string $type = null)
    {
        try {
            $posts = $this->feedRepository->getFeedPosts($type);

            return jsonResponse('success', 200, new PostCollection($posts));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}

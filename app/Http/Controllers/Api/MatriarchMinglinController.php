<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AppController;
use App\Http\Resources\UserSearchCollection;
use App\Repositories\MatriarchMinglin\MatriarchMinglinRepositoryInterface;

class MatriarchMinglinController extends AppController
{
    /**
     * MatriarchMinglin repository
     *
     * @var MatriarchMinglinRepositoryInterface
     */
    private $minglinRepository;

    /**
     * Constructor
     *
     * @param MatriarchMinglinRepositoryInterface $feedRepository Feed repository
     */
    public function __construct(MatriarchMinglinRepositoryInterface $minglinRepository)
    {
        $this->minglinRepository = $minglinRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $users = $this->minglinRepository->getMatriarchMinglin();

            return jsonResponse('success', 200, new UserSearchCollection($users));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}

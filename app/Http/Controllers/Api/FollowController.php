<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AppController;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Follow\FollowRepositoryInterface;
use App\Http\Resources\UserSearchCollection;
use App\Events\NewFollowerEvent;

class FollowController extends AppController
{
    /**
     * User repository
     *
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * Follow repository
     *
     * @var FollowRepositoryInterface
     */
    private $followRepository;

    /**
     * Constructor
     *
     * @param UserRepositoryInterface $userRepository User repository
     * @param FollowRepositoryInterface $followRepository Follow repository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        FollowRepositoryInterface $followRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->followRepository = $followRepository;
    }

    /**
     * Get user followers
     *
     * @return \Illuminate\Http\Response
     */
    public function followers($user_id)
    {
        try {
            $user = $this->userRepository->getUser($user_id);

            if(!$user) {
                return jsonResponse('error', 404, [
                    'message' => 'User not found.'
                ]);
            }

            $followers = $this->followRepository
                              ->getFollowers($user);

            return jsonResponse('success', 200, new UserSearchCollection($followers));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Get user followings
     *
     * @return \Illuminate\Http\Response
     */
    public function followings($user_id)
    {
        try {
            $user = $this->userRepository->getUser($user_id);

            if(!$user) {
                return jsonResponse('error', 404, [
                    'message' => 'User not found.'
                ]);
            }

            $followers = $this->followRepository
                              ->getFollowings($user);

            return jsonResponse('success', 200, new UserSearchCollection($followers));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Get user mutual followings
     *
     * @return \Illuminate\Http\Response
     */
    public function mutual($user_id)
    {
        try {
            $user = $this->userRepository->getUser($user_id);

            if(!$user) {
                return jsonResponse('error', 404, [
                    'message' => 'User not found.'
                ]);
            }

            $mutual = $this->followRepository
                           ->getMutual($user);

            return jsonResponse('success', 200, new UserSearchCollection($mutual));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Add new follower to user
     *
     * @return \Illuminate\Http\Response
     */
    public function follow($user_id)
    {
        try {
            $user = $this->userRepository->getUser($user_id);
            $follow = false;

            if(!$user) {
                return jsonResponse('error', 404, [
                    'message' => 'User not found.'
                ]);
            }

            $this->followRepository
                 ->followUser($user);

            event(new NewFollowerEvent($user));

            $follow = $this->followRepository
                           ->isAuthFollowing($user);

            return jsonResponse('success', 201, [
                'follow' => $follow
            ]);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Remove follower from user
     *
     * @return \Illuminate\Http\Response
     */
    public function unfollow($user_id)
    {
        try {
            $user = $this->userRepository->getUser($user_id);
            $follow = false;

            if(!$user) {
                return jsonResponse('error', 404, [
                    'message' => 'User not found.'
                ]);
            }

            $this->followRepository
                 ->unfollowUser($user);

            $follow = $this->followRepository
                           ->isAuthFollowing($user);

            return jsonResponse('success', 201, [
                'follow' => $follow
            ]);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        try {
            $follower = $this->userRepository->getUser($user_id);

            if(!$follower) {
                return jsonResponse('error', 404, [
                    'message' => 'User not found.'
                ]);
            }

            $this->followRepository
                 ->removeFollower($follower);

           $following = $this->followRepository
                             ->isAuthFollower($follower);

            return jsonResponse('success', 200, [
                'following' => $following
            ]);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}

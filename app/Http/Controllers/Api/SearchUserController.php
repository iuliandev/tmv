<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AppController;
use App\Http\Resources\UserSearchCollection;
use App\Repositories\Search\SearchRepositoryInterface;

class SearchUserController extends AppController
{
    /**
     * User repository
     *
     * @var SearchRepositoryInterface
     */
    private $searchUserRepository;

    /**
     * Constructor
     *
     * @param SearchRepositoryInterface $searchUserRepository
     */
    public function __construct(SearchRepositoryInterface $searchUserRepository)
    {
        $this->searchUserRepository = $searchUserRepository;
    }

     /**
      * Search users
      *
      * @param  Request  $request
      *
      * @return UserSearchCollection
      */
     public function search(Request $request)
     {
        try {
            $users = $this->searchUserRepository->search($request);

            return jsonResponse('success', 200, new UserSearchCollection($users));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Resources\UserSearchCollection;
use App\Http\Controllers\Api\AppController;
use App\Repositories\Comment\CommentRepositoryInterface;
use App\Repositories\LikeComment\LikeCommentRepositoryInterface;
use App\Events\NewCommentLikeEvent;

class LikeCommentController extends AppController
{
    /**
     * LikeComment repository
     *
     * @var LikeCommentRepositoryInterface
     */
    private $likeCommentRepository;

    /**
     * Comment repository
     *
     * @var CommentRepositoryInterface
     */
    private $commentRepository;

    /**
     * Constructor
     *
     * @param LikeCommentRepositoryInterface $likeCommentRepository
     */
    public function __construct(
        LikeCommentRepositoryInterface $likeCommentRepository,
        CommentRepositoryInterface $commentRepository
    )
    {
        $this->likeCommentRepository = $likeCommentRepository;
        $this->commentRepository = $commentRepository;
    }

    /**
     * Like comment
     *
     * @param  int  $comment_id
     * @return \Illuminate\Http\Response
     */
    public function toggleLike(int $comment_id)
    {
        try {
            $comment = $this->commentRepository
                            ->getComment($comment_id);

            if(!$comment) {
                return jsonResponse('error', 404, [
                    'message' => 'Comment not found.'
                ]);
            }

            $liked = false;
            if($comment->user_id != auth()->user()->id) {
                $liked = $this->likeCommentRepository
                              ->toggleLikeComment($comment);

                if($liked) {
                    event(new NewCommentLikeEvent($comment->user));
                }
            }

            return jsonResponse('success', 200, [
                'liked' => $liked
            ]);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Get users who reacted to comment
     *
     * @param  int  $comment_id
     * @return User
     */
    public function likes(int $comment_id)
    {
        try {
            $comment = $this->commentRepository
                            ->getComment($comment_id);

            if(!$comment) {
                return jsonResponse('error', 404, [
                    'message' => 'Comment not found.'
                ]);
            }

            $likes = $this->likeCommentRepository
                            ->getLikes($comment);

            return jsonResponse('success', 200, new UserSearchCollection($likes));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

}

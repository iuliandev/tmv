<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\User\UserRepositoryInterface;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserUpdatePassRequest;
use App\Http\Requests\UserUpdatePositionRequest;
use App\Http\Controllers\Api\AppController;
use App\Http\Resources\User as UserResource;
use App\User;

class UserController extends AppController
{
   /**
    * User repository
    *
    * @var UserRepositoryInterface
    */
    private $userRepository;

    /**
     * Constructor
     *
     * @param UserRepositoryInterface $userRepository User repository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display auth user resource.
     *
     * @return UserResource
     */
     public function auth()
     {
         try {
             $user = $this->userRepository
                          ->getAuthUser();

             return jsonResponse('success', 200, new UserResource($user));
         } catch(\Exception $e) {
             \Log::info(debugInfo($e));

             return jsonResponse('error', 500, $this->defaultErrorMessage);
         }
     }

   /**
    * Display user resource.
    *
    * @param  int  $id
    *
    * @return UserResource
    */
    public function show($id = null)
    {
        try {
            $user = $this->userRepository
                         ->getUser($id);

            if(!$user) {
                return jsonResponse('error', 404, [
                    'message' => 'User not found.'
                ]);
            }

            return jsonResponse('success', 200, new UserResource($user));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Update the user resource in storage.
     *
     * @param  App\Http\Requests\UserUpdateRequest  $request
     *
     * @return UserResource
     */
    public function update(UserUpdateRequest $request)
    {
        try {
            $this->userRepository
                 ->update($request);

            $user = $this->userRepository
                         ->getAuthUser();

            return jsonResponse('success', 200, new UserResource($user));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Update the user password.
     *
     * @param  App\Http\Requests\UserUpdatePassRequest  $request
     *
     * @return UserResource
     */
    public function updatePassword(UserUpdatePassRequest $request)
    {
        try {
            $this->userRepository
                 ->updatePassword($request);

            return jsonResponse('success', 200, [
                'message' => 'Password was changed.'
            ]);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Update the user position
     *
     * @param  UserUpdatePositionRequest  $request
     *
     * @return array
     */
    public function updatePosition(UserUpdatePositionRequest $request)
    {
        try {
            $this->userRepository
                 ->updatePosition($request);

            $user = $this->userRepository->getAuthUser();

            return jsonResponse('success', 200, [
                'position' => $user->position
            ]);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}

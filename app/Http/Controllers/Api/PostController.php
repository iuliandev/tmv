<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AppController;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Repositories\Post\PostRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Http\Resources\Post as PostResource;
use App\Http\Resources\PostCollection;

class PostController extends AppController
{
    /**
     * Post repository
     *
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * User repository
     *
     * @var UserRepositoryInterface
     */
    private $userRepository;


    /**
     * Constructor
     *
     * @param PostRepositoryInterface $postRepository Post repository
     * @param UserRepositoryInterface $userRepository User repository
     */
    public function __construct(
        PostRepositoryInterface $postRepository,
        UserRepositoryInterface $userRepository
    )
    {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        try {
            if(!$id) {
                $user = $this->userRepository->getAuthUser();
            } else {
                $user = $this->userRepository->getUser($id);

                if(!$user) {
                    return jsonResponse('error', 404, [
                        'message' => 'User not found.'
                    ]);
                }
            }

            $posts = $this->postRepository->all($user);

            return jsonResponse('success', 200, new PostCollection($posts));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\PostCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request)
    {
        try {
            if(!$request->text && !$request->media) {
                return jsonResponse('error', 412, [
                    'message' => 'Empty fields.'
                ]);
            }

            $post = $this->postRepository->create($request);

            if(!$post) {
                return jsonResponse('error', 400, $this->defaultErrorMessage);
            }

            return jsonResponse('success', 201, new PostResource($post));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $post = $this->postRepository->getPost($id);

            if(!$post) {
                return jsonResponse('error', 404, [
                    'message' => 'Post not found.'
                ]);
            }

            return jsonResponse('success', 200, new PostResource($post));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PostUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, int $id)
    {
        try {
            $user = $this->userRepository->getAuthUser();

            if(!$user->posts->contains($id)) {
                return jsonResponse('error', 404, [
                    'message' => 'Post not found.'
                ]);
            }

            $post = $this->postRepository->getPost($id);
            $this->postRepository->updatePost($post, $request);

            return jsonResponse('success', 200, new PostResource($post));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = $this->userRepository->getAuthUser();

            if(!$user->posts->contains($id)) {
                return jsonResponse('error', 404, [
                    'message' => 'Post not found.'
                ]);
            }

            $this->postRepository->removePost($id);

            return jsonResponse('success', 200, []);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}

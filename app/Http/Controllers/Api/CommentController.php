<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AppController;
use App\Repositories\Comment\CommentRepositoryInterface;
use App\Repositories\Post\PostRepositoryInterface;
use App\Http\Resources\Comment as CommentResource;
use App\Http\Resources\CommentCollection;
use App\Http\Requests\CommentRequest;
use App\Events\NewCommentReplyEvent;
use App\Events\NewCommentEvent;

class CommentController extends AppController
{
    /**
     * Comment repository
     *
     * @var CommentRepositoryInterface
     */
    private $commentRepository;

    /**
     * Post repository
     *
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * Constructor
     *
     * @param CommentRepositoryInterface $commentRepository Comment repository
     * @param PostRepositoryInterface $postRepository Post repository
     */
    public function __construct(
        CommentRepositoryInterface $commentRepository,
        PostRepositoryInterface $postRepository
    )
    {
        $this->commentRepository = $commentRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        try {
            $post = $this->postRepository->getPost($id);

            if(!$post) {
                return jsonResponse('error', 404, [
                    'message' => 'Post not found.'
                ]);
            }

            $postComments = $this->commentRepository->getPostComments($post);

            return jsonResponse('success', 200, new CommentCollection($postComments));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CommentRequest  $request
     * @param  int  $post_id
     * @param  int  $parent_id
     * @return \Illuminate\Http\Response
     */
    public function store(
        CommentRequest $request,
        int $post_id,
        int $parent_id = null
    )
    {
        try {
            $post = $this->postRepository->getPost($post_id);

            if(!$post) {
                return jsonResponse('error', 404, [
                    'message' => 'Post not found.'
                ]);
            }

            $auth_user = auth()->user();
            $comment = $this->commentRepository
                            ->addComment($request, $post_id, $parent_id);

            if($post->author->id != $auth_user->id)
                event(new NewCommentEvent($post->author));

            if($comment->parent && $comment->parent->user_id != $auth_user->id)
                event(new NewCommentReplyEvent($comment->parent->user));

            return jsonResponse('success', 201, new CommentResource($comment));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Update my comment.
     *
     * @param  CommentRequest  $request
     * @param  int  $comment_id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, int $comment_id)
    {
        try {
            $comment = $this->commentRepository->getComment($comment_id, true);

            if(!$comment) {
                return jsonResponse('error', 404, [
                    'message' => 'Comment not found.'
                ]);
            }

            $this->commentRepository->updateComment($request, $comment);

            return jsonResponse('success', 201, new CommentResource($comment));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $comment_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $comment_id)
    {
        try {
            $comment = $this->commentRepository->getComment($comment_id, true);

            if(!$comment) {
                return jsonResponse('error', 404, [
                    'message' => 'Comment not found.'
                ]);
            }

            $this->commentRepository->removeComment($comment);

            return jsonResponse('success', 200, []);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}

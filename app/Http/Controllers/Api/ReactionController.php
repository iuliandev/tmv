<?php

namespace App\Http\Controllers\Api;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\AppController;
use App\Http\Resources\Post as PostResource;
use App\Events\NewReactionEvent;
use App\Http\Resources\ReactionCollection;
use App\Http\Resources\UserSearchCollection;
use App\Repositories\Post\PostRepositoryInterface;
use App\Repositories\Reaction\ReactionRepositoryInterface;

class ReactionController extends AppController
{
    /**
     * Post repository
     *
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * Reaction repository
     *
     * @var ReactionRepositoryInterface
     */
    private $reactionRepository;


    /**
     * Constructor
     *
     * @param PostRepositoryInterface $postRepository Post repository
     * @param ReactionRepositoryInterface $reactionRepository Post React repository
     */
    public function __construct(
        PostRepositoryInterface $postRepository,
        ReactionRepositoryInterface $reactionRepository
    )
    {
        $this->postRepository = $postRepository;
        $this->reactionRepository = $reactionRepository;
    }

    /**
     * React to post
     *
     * @param  int  $post_id  Post id
     * @param  string  $type  type of reaction
     *
     * @return PostResource
     */
    public function react(int $post_id, string $type)
    {
        try {
            $post = $this->postRepository->getPost($post_id);

            if(!$post) {
                return jsonResponse('error', 404, [
                    'message' => 'Post not found.'
                ]);
            }

            $this->reactionRepository->toggleReaction($post, $type);

            event(new NewReactionEvent($post->author));

            return jsonResponse('success', 200, new PostResource($post));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Get users who reacted to post
     *
     * @param  int  $id  Post id
     * @param  string  $type  type of reaction
     *
     * @return PostResource
     */
    public function reactions(int $post_id)
    {
        try {
            $post = $this->postRepository->getPost($post_id);

            if(!$post) {
                return jsonResponse('error', 404, [
                    'message' => 'Post not found.'
                ]);
            }

            $reactions = $this->reactionRepository->getReactions($post);

            return jsonResponse('success', 200, new ReactionCollection($reactions));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}

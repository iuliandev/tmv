<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserSearch;
use App\Http\Resources\CommentCollection;
use App\Comment as CommentModel;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $parent = CommentModel::find($this->parent_id);

        $array = [
            'id'           => $this->id,
            'post_id'      => $this->post_id,
            'parent_id'    => $this->parent_id,
            'comment'      => $this->comment,
            'likes'        => $this->likes()->count(),
            'liked'        => $this->likes->contains('pivot.user_id', auth()->user()->id),
            'created_at'   => $this->created_at->diffForHumans(),
            'user'         => [
                'id'          => $this->user->id,
                'firstname'   => $this->user->firstname,
                'lastname'    => $this->user->lastname,
                'position'    => $this->user->position,
                'avatar'      => mediaFile('users', $this->user->avatar),
                'url'         => url('/') . '/api/user/'.$this->user->id
            ]
        ];

        if($parent) {
            $array['user_parent'] = [
                'id'          => $parent->user->id,
                'firstname'   => $parent->user->firstname,
                'lastname'    => $parent->user->lastname,
                'position'    => $parent->user->position,
                'avatar'      => mediaFile('users', $parent->user->avatar),
                'url'         => url('/') . '/api/user/'.$parent->user->id
            ];
        }

        if($this->replies) {
            $array['replies'] = new CommentCollection($this->replies);
        }

        return $array;
    }
}

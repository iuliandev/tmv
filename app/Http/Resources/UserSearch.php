<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserSearch extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $follow = auth()->user()->followings->contains($this->id);
        $following = $this->followings->contains(auth()->user()->id);

        return [
            'id'                   => $this->id,
            'firstname'            => $this->firstname,
            'lastname'             => $this->lastname,
            'email'                => $this->email,
            'age'                  => $this->age,
            'zip_code'             => $this->zip_code,
            'position'             => $this->position,
            'follow'               => $follow,
            'following'            => $following,
            'mom_status'           => $this->mom_status->title ?? null,
            'mom_hustle'           => $this->mom_hustle->title ?? null,
            'short_bio'            => $this->short_bio,
            'avatar'               => mediaFile('users', $this->avatar),
            'created_at'           => $this->created_at->diffForHumans(),
            'url'                  => url('/') . '/api/user/'.$this->id
        ];
    }
}

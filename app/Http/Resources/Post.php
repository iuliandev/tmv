<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserSearchCollection;
use App\Http\Resources\UserSearch;

class Post extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $tagged_users = $this->with_users()->get();
        $reacted = $this->reactions()
                        ->where('reactions.user_id', auth()->user()->id)
                        ->first();
        $subcoments = $this->comments()
                           ->whereHas('replies')
                           ->count();

        return [
            'id'          => $this->id,
            'type'        => $this->type,
            'title'       => $this->title,
            'text'        => $this->text,
            'media'       => mediaFile('posts', $this->media),
            'feeling'     => $this->feeling,
            'location'    => $this->location,
            'lat'         => $this->lat,
            'lng'         => $this->lng,
            'created_at'  => $this->created_at->diffForHumans(),
            'comments'    => $this->comments()->count() + $subcoments,
            'url'         => url('/') . '/api/posts/'.$this->id,
            'reacted'     => $reacted ? true : false,
            'reaction'    => $reacted ? $reacted->reaction : null,
            'reactions_total' => $this->reactions->count(),
            'reactions'   => [
                'like' => $this->reactions_like()->count(),
                'haha' => $this->reactions_haha()->count(),
                'love' => $this->reactions_love()->count()
            ],
            'author'      => new UserSearch($this->author),
            'tagged'      => new UserSearchCollection($tagged_users)
        ];
    }
}

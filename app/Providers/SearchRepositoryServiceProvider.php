<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SearchRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Search\SearchRepositoryInterface',
            'App\Repositories\Search\SearchRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

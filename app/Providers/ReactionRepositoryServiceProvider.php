<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ReactionRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Reaction\ReactionRepositoryInterface',
            'App\Repositories\Reaction\ReactionRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

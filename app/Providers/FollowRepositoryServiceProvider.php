<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FollowRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Follow\FollowRepositoryInterface',
            'App\Repositories\Follow\FollowRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

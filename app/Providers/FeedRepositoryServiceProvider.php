<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FeedRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Feed\FeedRepositoryInterface',
            'App\Repositories\Feed\FeedRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

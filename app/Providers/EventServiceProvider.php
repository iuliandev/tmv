<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \App\Events\NewCommentEvent::class => [
            \App\Listeners\NewCommentPushListener::class,
            \App\Listeners\NewCommentNotifyListener::class,
        ],
        \App\Events\NewCommentReplyEvent::class => [
            \App\Listeners\NewCommentReplyPushListener::class,
            \App\Listeners\NewCommentReplyNotifyListener::class,
        ],
        \App\Events\NewCommentLikeEvent::class => [
            \App\Listeners\NewCommentLikePushListener::class,
            \App\Listeners\NewCommentLikeNotifyListener::class,
        ],
        \App\Events\NewFollowerEvent::class => [
            \App\Listeners\NewFollowerPushListener::class,
            \App\Listeners\NewFollowerNotifyListener::class,
        ],
        \App\Events\NewReactionEvent::class => [
            \App\Listeners\NewReactionPushListener::class,
            \App\Listeners\NewReactionNotifyListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LikeCommentRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\LikeComment\LikeCommentRepositoryInterface',
            'App\Repositories\LikeComment\LikeCommentRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

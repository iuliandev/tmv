<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MinglinRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\MatriarchMinglin\MatriarchMinglinRepositoryInterface',
            'App\Repositories\MatriarchMinglin\MatriarchMinglinRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Repositories\MatriarchMinglin;

use Illuminate\Http\Request;

interface MatriarchMinglinRepositoryInterface
{
    public function getMatriarchMinglin();
}

<?php

namespace App\Repositories\MatriarchMinglin;

use App\User;
use App\Repositories\MatriarchMinglin\MatriarchMinglinRepositoryInterface;

class MatriarchMinglinRepository implements MatriarchMinglinRepositoryInterface
{
    private $user;

    private $items;

    /**
     * Constructor
     *
     * @param User $user User entity
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->items = 15;
    }

    /**
     * Get recommended matriarchs
     *
     * @return User
     */
    public function getMatriarchMinglin()
    {
        $auth = auth()->user();
        $exclude_ids = array($auth->id);

        $auth_followings = $auth->followings
                                ->pluck('id')
                                ->toArray();

        if($auth_followings) {
            array_push($exclude_ids, ...$auth_followings);
        }

        $users = $this->user->whereNotIn('id', $exclude_ids)
                            ->inRandomOrder('1234')
                            ->paginate(9);

        return $users;
    }
}

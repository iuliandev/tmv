<?php

namespace App\Repositories\Reaction;

use App\Post;
use App\User;
use App\Reaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Reaction\ReactionRepositoryInterface;

class ReactionRepository implements ReactionRepositoryInterface
{
    private $reaction;

    private $user;

    /**
     * Constructor
     *
     * @param Reaction $reaction Reaction entity
     */
    public function __construct(
        Reaction $reaction,
        User $user
    )
    {
        $this->reaction = $reaction;
        $this->user = $user;
    }

    /**
     * Add/Update/Remove reaction to post
     *
     * @param Post $post
     * @param string $type
     */
    public function toggleReaction(Post $post, string $type)
    {
        $reaction = $this->reaction->findReaction($post);

        if($reaction) {
            $this->reaction->updateOrDetachReaction($reaction, $type);
        } else {
            $this->reaction->addReaction($post, $type);
        }
    }

    /**
     * Get reacted users
     *
     * @param Post $post
     * @return User
     */
    public function getReactions(Post $post)
    {
        $reactions = $post->reactions()
                          ->orderBy('created_at', 'desc')
                          ->paginate(10);

        return $reactions;
    }
}

<?php

namespace App\Repositories\Reaction;

use App\Post;
use Illuminate\Http\Request;

interface ReactionRepositoryInterface
{
    public function toggleReaction(Post $post, string $type);

    public function getReactions(Post $post);
}

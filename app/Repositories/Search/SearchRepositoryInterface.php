<?php

namespace App\Repositories\Search;

use App\User;
use Illuminate\Http\Request;

interface SearchRepositoryInterface
{
    public function search(Request $request);
}

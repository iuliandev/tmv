<?php

namespace App\Repositories\Search;

use App\User;
use Illuminate\Http\Request;
use App\Repositories\Search\SearchRepositoryInterface;

class SearchRepository implements SearchRepositoryInterface
{
    private $user;

   /**
    * Constructor
    *
    * @param User $user User entity
    */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Search users
     *
     * @param Request $request
     *
     * @return User
     */
    public function search(Request $request)
    {
        $name = $request->input('name');
        $users = $this->user;

        if(isset($name) && !empty($name)) {
            $users = $this->user->searchUsers($name);
        }

        $users = $users->orderBy('created_at', 'desc')
                       ->paginate(15);

        return $users;
    }
}

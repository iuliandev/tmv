<?php

namespace App\Repositories\Feed;

use App\Post;
use Illuminate\Http\Request;

interface FeedRepositoryInterface
{
    public function getFeedPosts(string $type);
}

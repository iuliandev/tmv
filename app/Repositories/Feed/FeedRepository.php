<?php

namespace App\Repositories\Feed;

use App\Post;
use App\Repositories\Feed\FeedRepositoryInterface;

class FeedRepository implements FeedRepositoryInterface
{
    private $post;

    /**
     * Constructor
     *
     * @param Post $post Post entity
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Get feed posts
     *
     * @param stirng $type
     *
     * @return Post
     */
    public function getFeedPosts(string $type)
    {
        $posts = $this->post;

        if($type == 'recent') {
            $posts = $this->post->recentPosts();
        }

        if($type == 'top') {
            $posts = $this->post->topPosts();
        }

        if($type == 'following') {

        }

        $posts = $posts->paginate(10);

        return $posts;
    }
}

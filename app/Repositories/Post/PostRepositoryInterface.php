<?php

namespace App\Repositories\Post;

use App\Post;
use Illuminate\Http\Request;

interface PostRepositoryInterface
{
    public function create(Request $request);

    public function updatePost(Post $post, Request $request);

    public function getPost(int $id);

    public function removePost(int $id);
}

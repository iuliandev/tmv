<?php

namespace App\Repositories\Post;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Post\PostRepositoryInterface;

class PostRepository implements PostRepositoryInterface
{
    private $post;

    private $onlyFields = array(
        'type',
        'title',
        'text',
        'media',
        'feeling',
        'location',
        'lat',
        'lng',
        'visibility',
        'with_user'
    );

    /**
     * Constructor
     *
     * @param Post $post Post entity
     */
    public function __construct(
        Post $post,
        User $user
    )
    {
        $this->post = $post;
        $this->user = $user;
    }

    /**
     * Create post
     *
     * @param Request $request
     *
     * @return Post
     */
    public function create(Request $request)
    {
        $user = auth()->user();
        $fields = $request->only($this->onlyFields);

        if($request->media) {
            $fields['media'] = $this->addMedia($request->media);
        }

        $post = $user->posts()->create($fields);

        if($post && $request->with_user) {
            $post->with_users()->attach($request->with_user);
        }

        return $post;
    }

    /**
     * Update post
     *
     * @param Request $request
     *
     * @param int $id
     *
     * @return Post
     */
    public function updatePost(Post $post, Request $request)
    {
        $fields = $request->except('media');
        $media_text = $request->input('media');
        $media_file = $request->file('media');

        if($media_text == null && $media_file == null) {
            $this->removePostMedia($post->media);
            $fields['media'] = null;
        } else if($media_file) {
            $this->removePostMedia($post->media);
            $fields['media'] = $this->addMedia($request->media);
        }

        $post->update($fields);
    }

    /**
     * Get user posts
     *
     *  @param User $user
     *
     * @return Post
     */
    public function all(User $user)
    {
        $posts = $user->posts()
                      ->orderBy('created_at', 'desc')
                      ->paginate(10);

        return $posts;
    }

    /**
     * Get single post
     *
     *  @param int $id
     *
     * @return Post
     */
    public function getPost(int $id)
    {
        $post = $this->post
                     ->where('id', $id)
                     ->first();

        return $post;
    }

    /**
     * Delete post
     *
     *  @param int $id
     */
    public function removePost(int $id)
    {
        $post = auth()->user()
                      ->posts()
                      ->where('id', $id)
                      ->first();

        if($post->media) {
            $this->removePostMedia($post->media);
        }

        $post->delete();
    }

    /**
     * Add post media file
     *
     * @param $media
     *
     * @return string
     */
    private function addMedia($media)
    {
        $file_name = $media->getClientOriginalName();
        $folder_name = time().mt_rand(1, 999999);

        $file_name = str_replace(' ', '', $file_name);

        $image = Storage::disk('local')->putFileAs(
            'posts/'.$folder_name,
            $media,
            $file_name
        );

        $media_url = $folder_name . '/' . $file_name;

        return $media_url;
    }

    /**
     * Remove post media file
     *
     * @param string
     *
     */
    private function removePostMedia($media_url)
    {
        $folder = strtok($media_url, '/');

        if($folder) {
            Storage::deleteDirectory('posts/'.$folder);
        }
    }
}

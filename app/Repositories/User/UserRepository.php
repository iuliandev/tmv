<?php

namespace App\Repositories\User;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserUpdatePassRequest;
use App\Repositories\User\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    private $user;

   /**
    * Constructor
    *
    * @param User $user User entity
    */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

   /**
    * Get user by id
    *
    * @param int $id
    *
    * @return User
    */
    public function getUser(int $id = null)
    {
        if($id !== null) {
            $user = $this->user::find($id);

            return $user;
        }

        return null;
    }

    /**
     * Get auth user
     *
     * @return User
     */
     public function getAuthUser()
     {
         return auth()->user();
     }

   /**
    * Update user
    *
    * @param Request $request
    *
    * @return User
    */
    public function update(Request $request)
    {
        $user = auth()->user();
        $fields = $request->except('avatar');
        $avatar_text = $request->input('avatar');
        $avatar_file = $request->file('avatar');

        if($avatar_text == null && $avatar_file == null) {
            $this->removeCurrentAvatar();
            $fields['avatar'] = 'default/avatar.png';
        } else if($avatar_file) {
            $fields['avatar'] = $this->updateAvatar($avatar_file);
        }

        $user->update($fields);

        return $user;
    }

   /**
    * Update user password
    *
    * @param Request $request
    *
    * @return User
    */
    public function updatePassword(Request $request)
    {
        $user = auth()->user();

        $user->setPassword(Hash::make($request->password));

        return $user;
    }

    /**
     * Update user position
     *
     * @param Request $request
     *
     */
     public function updatePosition(Request $request)
     {
         $user = auth()->user();

         $user = $user->setPosition($request->position);
         $user->save();
     }

     /**
      * Update firebase app token
      *
      * @param string $app_token
      *
      * @return void
      */
     public function updateAppToken(string $app_token = null)
     {
         $user = auth()->user();

         $user = $user->setAppToken($app_token);
         $user->save();
     }

    /**
     * Update user avatar
     *
     * @param $avatar
     *
     * @return string
     */
    private function updateAvatar($avatar)
    {
        $file_name = $avatar->getClientOriginalName();
        $folder_name = time().mt_rand(1, 999999);

        $image = Storage::disk('local')->putFileAs(
            'users/'.$folder_name,
            $avatar,
            $file_name
        );

        if($image) {
            $this->removeCurrentAvatar();
        }

        $image_url = $folder_name . '/' . $file_name;

        return $image_url;
    }

    /**
     * Remove curent user avatar
     *
     * @return void
     */
    private function removeCurrentAvatar()
    {
        $image = auth()->user()->avatar;

        if(!empty($image) && $image != 'default/avatar.png') {
            $folder = strtok($image, '/');
            Storage::deleteDirectory('users/' . $folder);
        }
    }
}

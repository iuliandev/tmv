<?php

namespace App\Repositories\User;

use App\User;
use Illuminate\Http\Request;

interface UserRepositoryInterface
{
    public function getUser(int $id);

    public function getAuthUser();

    public function update(Request $request);

    public function updatePosition(Request $request);
}

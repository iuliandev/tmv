<?php

namespace App\Repositories\LikeComment;

use App\Comment;
use App\Repositories\LikeComment\LikeCommentRepositoryInterface;

class LikeCommentRepository implements LikeCommentRepositoryInterface
{
    /**
     * Toggle like to comment
     *
     * @param Comment $comment
     *
     */
    public function toggleLikeComment(Comment $comment)
    {
        $liked = $comment->likes
                         ->contains('pivot.user_id', auth()->user()->id);

        if($liked) {
            $comment->likes()
                    ->detach(auth()->user()->id);
        } else {
            $comment->likes()
                    ->attach(auth()->user()->id);
        }

        return !$liked;
    }

    /**
     * Get comment likes
     *
     * @param Comment $comment
     * @return User
     */
    public function getLikes(Comment $comment)
    {
        $likes = $comment->likes()
                         ->orderBy('created_at', 'desc')
                         ->paginate(10);

        return $likes;
    }
}

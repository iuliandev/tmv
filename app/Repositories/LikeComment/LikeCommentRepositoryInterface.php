<?php

namespace App\Repositories\LikeComment;

use App\Comment;
use Illuminate\Http\Request;

interface LikeCommentRepositoryInterface
{
    public function toggleLikeComment(Comment $comment);

    public function getLikes(Comment $comment);
}

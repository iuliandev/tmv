<?php

namespace App\Repositories\Follow;

use App\User;
use App\Repositories\Follow\FollowRepositoryInterface;

class FollowRepository implements FollowRepositoryInterface
{
    private $user;

    private $items;

    /**
     * Constructor
     *
     * @param User $user User entity
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->items = 15;
    }

    /**
     * Follow user
     *
     * @param User $user
     */
    public function followUser(User $user)
    {
        $user->followers()
             ->sync([auth()->user()->id], false);
    }

    /**
     * Unfollow user
     *
     * @param User $user
     */
    public function unfollowUser(User $user)
    {
        auth()->user()
              ->followings()
              ->detach($user->id);
    }

    /**
     * Remove follower
     *
     * @param User $user
     */
    public function removeFollower(User $user)
    {
        auth()->user()
              ->followers()
              ->detach($user->id);
    }

    /**
     * Chech if auth user is follow $user
     *
     * @param User $user
     *
     * @return bool
     */
    public function isAuthFollowing(User $user)
    {
        $following = auth()->user()
                           ->followings
                           ->contains($user->id);

        return $following;
    }

    /**
     * Chech if $user is follower of auth user
     *
     * @param User $user
     *
     * @return bool
     */
    public function isAuthFollower(User $user)
    {
        $follower = auth()->user()
                          ->followers
                          ->contains($user->id);

        return $follower;
    }

    /**
     * Get user followers
     *
     * @param User $user
     *
     * @return array
     */
    public function getFollowers(User $user)
    {
        $followers = $user->followers()
                          ->orderBy('followers.created_at', 'desc')
                          ->paginate($this->items);

        return $followers;
    }

    /**
     * Get user followings
     *
     * @param User $user
     *
    * @return array
     */
    public function getFollowings(User $user)
    {
        $followings = $user->followings()
                           ->orderBy('followers.created_at', 'desc')
                           ->paginate($this->items);

        return $followings;
    }

    /**
     * Get user mutual followings
     *
     * @param User $user
     *
    * @return array
     */
    public function getMutual(User $user)
    {
        $user_followings = $user->followings
                                ->pluck('id')
                                ->toArray();

        $auth_followings = auth()->user()
                                 ->followings
                                 ->pluck('id')
                                 ->toArray();

        $mutual_ids = array_intersect($user_followings, $auth_followings);

        $mutual_connections = $this->user
                                   ->whereIn('id', $mutual_ids)
                                   ->paginate($this->items);

        return $mutual_connections;
    }
}

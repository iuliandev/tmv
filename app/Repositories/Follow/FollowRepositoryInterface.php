<?php

namespace App\Repositories\Follow;

use App\User;
use Illuminate\Http\Request;

interface FollowRepositoryInterface
{
    public function followUser(User $user);

    public function unfollowUser(User $user);

    public function removeFollower(User $user);

    public function isAuthFollowing(User $user);

    public function isAuthFollower(User $user);

    public function getFollowers(User $user);

    public function getFollowings(User $user);

    public function getMutual(User $user);
}

<?php

namespace App\Repositories\Comment;

use App\Post;
use App\Comment;
use Illuminate\Http\Request;

interface CommentRepositoryInterface
{
    public function addComment(Request $request, int $post_id, int $parent_id);

    public function getPostComments(Post $post);

    public function getComment(int $comment_id, bool $myself);

    public function updateComment(Request $request, Comment $comment);

    public function removeComment(Comment $comment);
}

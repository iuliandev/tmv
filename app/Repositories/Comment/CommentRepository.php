<?php

namespace App\Repositories\Comment;

use App\Post;
use App\Comment;
use Illuminate\Http\Request;
use App\Repositories\Comment\CommentRepositoryInterface;

class CommentRepository implements CommentRepositoryInterface
{
    private $comment;

    /**
     * Constructor
     *
     * @param Comment $comment Comment entity
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Add comment to post
     *
     * @param Request $request
     * @param int $post_id
     * @param int $parent_id
     *
     * @return Comment
     */
    public function addComment(Request $request, int $post_id, int $parent_id = null)
    {
        $comment = $this->comment->create(array(
            'user_id' => auth()->user()->id,
            'post_id' => $post_id,
            'parent_id' => $parent_id,
            'comment' => $request->comment
        ));

        return $comment;
    }

    /**
     * Get post comments
     *
     * @param Post $post
     *
     * @return Comment
     */
    public function getPostComments(Post $post)
    {
        $comments = $post->comments()
                         ->orderBy('created_at', 'desc')
                         ->paginate(15);

        return $comments;
    }

    /**
     * Get author comment
     *
     * @param int $comment_id
     * @param bool $myself
     *
     * @return Comment
     */
    public function getComment(int $comment_id, bool $myself = false)
    {
        $comment = $this->comment->find($comment_id);

        if($myself) {
            $comment = $this->comment->findMyComment($comment_id);
        }

        return $comment;
    }

    /**
     * Update comment
     *
     * @param Request $request
     * @param Comment $comment
     *
     * @return Comment
     */
    public function updateComment(Request $request, Comment $comment)
    {
        $comment->comment = $request->comment;
        $comment->save();

        return $comment;
    }

    /**
     * Delete comment
     *
     *  @param Comment $comment
     */
    public function removeComment(Comment $comment)
    {
        $comment->delete();
    }
}

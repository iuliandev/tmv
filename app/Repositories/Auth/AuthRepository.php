<?php

namespace App\Repositories\Auth;

use App\User;
use App\MomStatus;
use App\MomHustle;
use App\MatriarchLife;
use App\ChildrensActivity;
use App\ChildcareManagement;
use Carbon\Carbon;
use Laravel\Passport\Passport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Auth\AuthRepositoryInterface;

class AuthRepository implements AuthRepositoryInterface
{
    private $user,
            $mom_status,
            $mom_hustle,
            $matriarch_life,
            $childrens_activities,
            $childcare_management;

    /**
     * Constructor
     *
     * @param User $user User entity
     */
    public function __construct(
        User $user,
        MomStatus $mom_status,
        MomHustle $mom_hustle,
        MatriarchLife $matriarch_life,
        ChildrensActivity $childrens_activities,
        ChildcareManagement $childcare_management
    )
    {
        $this->user = $user;
        $this->mom_status = $mom_status;
        $this->mom_hustle = $mom_hustle;
        $this->matriarch_life = $matriarch_life;
        $this->childrens_activities = $childrens_activities;
        $this->childcare_management = $childcare_management;
    }

    /**
     * Get user by email
     *
     * @return User
     */
     public function getUserByEmail($email)
     {
         $user = $this->user::where('email', $email)->first();

         return $user;
     }

    /**
     * Populate first step user registration
     *
     * @param User $user User
     *
     * @return User
     */
    public function populateFirstStep(User $user, Request $request)
    {
        $user->setEmail($request->email)
             ->setPassword(Hash::make($request->password))
             ->setAppToken($request->app_token)
             ->setDefaultAvatar()
             ->save();
    }

    /**
     * Populate second step user registration
     *
     * @param User $user User
     *
     * @return User
     */
    public function populateSecondStep(Request $request)
    {
        $user = auth()->user();

        $user->setFirstname($request->firstname)
             ->setLastname($request->lastname)
             ->setAge($request->age)
             ->setZipCode($request->zip_code)
             ->setMomStatus($request->mom_status_id)
             ->setMomHustle($request->mom_status_id)
             ->setMatriarchLife($request->matriarch_life)
             ->setChildcareManagement($request->childcare_management)
             ->setChildrensActivities($request->childrens_activities)
             ->setMommingHard($request->momming_hard)
             ->save();

        return $user;
    }

    /**
     * Get registration grouped options
     *
     * @return void
     */
    public function getInputs() {
        $inputs = array();

        $inputs['MomBoss Status:']                   = $this->mom_status->getOptions();
        $inputs['What\'s your MomHustle?']           = $this->mom_hustle->getOptions();
        $inputs['Living the Matriarch Life:']        = $this->matriarch_life->getOptions();
        $inputs['Childrens Curiosities & Activity:'] = $this->childrens_activities->getOptions();
        $inputs['Childcare Managenemt:']             = $this->childcare_management->getOptions();

        return $inputs;
    }

    /**
     * Get user acces token
     *
     * @param User $user User
     *
     * @return string
     */
    public function getToken(User $user) {
        return $user->createToken('AppName')->accessToken;
    }

    /**
     * Set tokens expiration
     */
    public function setTokenExpiration()
    {
        Passport::tokensExpireIn(Carbon::now()->addYears(1));
        Passport::refreshTokensExpireIn(Carbon::now()->addYears(1));
    }

    /**
     * Delete user access token
     *
     * @return void
     */
    public function deleteToken() {
        auth()->user()->AauthAcessToken()->delete();
    }
}

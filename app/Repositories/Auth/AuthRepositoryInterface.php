<?php

namespace App\Repositories\Auth;

use App\User;
use Illuminate\Http\Request;

interface AuthRepositoryInterface
{
    public function populateFirstStep(User $user, Request $request);

    public function populateSecondStep(Request $request);

    public function getUserByEmail($email);

    public function getInputs();

    public function getToken(User $user);

    public function deleteToken();
}

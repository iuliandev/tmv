<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MommingHard
 *
 * @property int $id
 * @property int $nth_child
 * @property \Carbon\Carbon $birth_date
 * @property string $gender
 *
 * @package App\Models
 */
class MommingHard extends Model
{
		protected $table = 'momming_hard';
		public $timestamps = false;

		protected $casts = [
				'nth_child' => 'int',
		];

		protected $fillable = [
				'nth_child',
				'birth_date',
				'gender'
		];


		public function user()
		{
				return $this->belongsTo('App\User');
		}
}

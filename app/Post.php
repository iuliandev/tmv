<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'type',
        'title',
        'text',
        'media',
        'feeling',
        'location',
        'lat',
        'lng',
        'visibility',
        'removed_by_admin'
    ];

    /**
     * The data type attributes
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'updated_at',
        'created_at'
    ];

    public function author() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function with_users()
    {
        return $this->belongsToMany('App\User', 'post_with_users', 'post_id', 'user_id')
                    ->withTimestamps();
    }

    public function reactions()
    {
        return $this->hasMany('App\Reaction');
    }

    public function reactions_like()
    {
        return $this->hasMany('App\Reaction')
                    ->where('reactions.reaction', 'like');
    }

    public function reactions_haha()
    {
        return $this->hasMany('App\Reaction')
                    ->where('reactions.reaction', 'haha');
    }

    public function reactions_love()
    {
        return $this->hasMany('App\Reaction')
                    ->where('reactions.reaction', 'love');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment')
                    ->whereNull('parent_id');
    }

    public function recentPosts()
    {
        return $this->where('visibility', 'Public')
                    ->orderBy('created_at', 'desc');
    }

    public function topPosts()
    {
        return $this->where('visibility', 'Public')
                    ->withCount('reactions')
                    ->orderBy('reactions_count', 'desc');
    }


}

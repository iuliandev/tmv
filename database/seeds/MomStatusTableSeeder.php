<?php

use Illuminate\Database\Seeder;

class MomStatusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mom_status')->delete();
        
        \DB::table('mom_status')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Single',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Divorced',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Married',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Domestic Partner',
            ),
        ));
        
        
    }
}
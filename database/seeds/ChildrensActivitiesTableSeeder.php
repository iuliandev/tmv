<?php

use Illuminate\Database\Seeder;

class ChildrensActivitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('childrens_activities')->delete();
        
        \DB::table('childrens_activities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Sports',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Music',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Art',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Math',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Technology',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Science',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Reading',
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'Animals',
            ),
        ));
        
        
    }
}
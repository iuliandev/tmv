<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'firstname' => 'Britney',
                'lastname' => 'Cooper',
                'email' => 'test@gmail.com',
                'age' => 27,
                'zip_code' => '90245',
                'mom_status_id' => 3,
                'mom_hustle_id' => 2,
                'email_verified_at' => NULL,
                'password' => '$2y$10$4OrDeTY3iT68jDIdb9nGH.YVWnPbwAOmLKHTNWJ4z229TEldbc6lK',
                'remember_token' => NULL,
                'created_at' => '2019-10-02 13:56:24',
                'updated_at' => '2019-10-02 13:57:03',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
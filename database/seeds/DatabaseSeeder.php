<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OauthClientsTableSeeder::class);
        $this->call(OauthPersonalAccessClientsTableSeeder::class);
        $this->call(ChildcareManagementTableSeeder::class);
        $this->call(ChildrensActivitiesTableSeeder::class);
        $this->call(MatriarchLifeTableSeeder::class);
        $this->call(MomHustleTableSeeder::class);
        $this->call(MomStatusTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(OauthAccessTokensTableSeeder::class);
    }
}

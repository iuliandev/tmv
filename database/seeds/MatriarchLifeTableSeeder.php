<?php

use Illuminate\Database\Seeder;

class MatriarchLifeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('matriarch_life')->delete();
        
        \DB::table('matriarch_life')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Philantrophy',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Outdoorsy',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Yoga/Pilates/Barre',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Style & Beauty',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Cardio Queen',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Country Club',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Food & Drink',
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'Home & Garden',
            ),
            8 => 
            array (
                'id' => 9,
                'title' => 'Culture & Arts',
            ),
            9 => 
            array (
                'id' => 10,
                'title' => 'Current Events',
            ),
            10 => 
            array (
                'id' => 11,
                'title' => 'Business & Technology',
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class MomHustleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mom_hustle')->delete();
        
        \DB::table('mom_hustle')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Corporate Corner Office',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Remote Entrepreneur',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Momming Full-time',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Blissfully Retired',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Doting Grandparent',
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class ChildcareManagementTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('childcare_management')->delete();
        
        \DB::table('childcare_management')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Full-Time',
                'type' => 'Nanny',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Share',
                'type' => 'Nanny',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Live-in',
                'type' => 'Nanny',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Partial',
                'type' => 'Day Care',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Full-Time',
                'type' => 'Day Care',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Home',
                'type' => 'Day Care',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Public',
                'type' => 'School',
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'Private',
                'type' => 'School',
            ),
        ));
        
        
    }
}
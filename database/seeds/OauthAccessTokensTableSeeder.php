<?php

use Illuminate\Database\Seeder;

class OauthAccessTokensTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oauth_access_tokens')->delete();
        
        \DB::table('oauth_access_tokens')->insert(array (
            0 => 
            array (
                'id' => 'bd5a73d9479f14d13971e6502e8ba6f61aeefaf290cf95598342ce9fdd0fb5903129bd68dbcedc1d',
                'user_id' => 1,
                'client_id' => 1,
                'name' => 'AppName',
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2019-10-02 13:56:24',
                'updated_at' => '2019-10-02 13:56:24',
                'expires_at' => '2020-10-02 13:56:24',
            ),
        ));
        
        
    }
}
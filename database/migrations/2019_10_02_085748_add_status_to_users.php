<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->biginteger('mom_status_id')->after('zip_code')->unsigned()->nullable();
            $table->biginteger('mom_hustle_id')->after('mom_status_id')->unsigned()->nullable();

            $table->foreign('mom_status_id')->references('id')->on('mom_status')->onDelete('cascade');
            $table->foreign('mom_hustle_id')->references('id')->on('mom_hustle')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login',                         'Api\AuthController@login');
Route::post('register/step/1',               'Api\AuthController@register');

Broadcast::routes(['middleware' => 'auth:api']);

Route::group(['middleware' => 'auth:api'], function() {
    Route::namespace('Api')->group(function () {
        Route::get('register-inputs',                  'AuthController@inputs');
        Route::post('register/step/2',                 'AuthController@registerContinue');

        Route::get('user',                             'UserController@auth');
        Route::get('user/{id}',                        'UserController@show');
        Route::post('user/update',                     'UserController@update');
        Route::post('user/update-password',            'UserController@updatePassword');
        Route::post('user/update-position',            'UserController@updatePosition');

        Route::post('logout',                          'AuthController@logout');

        Route::get('users/search',                     'SearchUserController@search');

        Route::get('posts',                            'PostController@index');
        Route::get('posts/{id}',                       'PostController@show');
        Route::get('user/{id?}/posts',                 'PostController@index');
        Route::post('posts/store',                     'PostController@store');
        Route::post('posts/{id}/update',               'PostController@update');
        Route::delete('posts/{id}/destroy',            'PostController@destroy');

        Route::get('feed/{type}',                      'FeedController@index');

        Route::post('posts/{post_id}/{react}',         'ReactionController@react');
        Route::get('posts/{post_id}/reactions',        'ReactionController@reactions');

        Route::post('comments/post/{post_id}/store/{parrent_id?}',         'CommentController@store');
        Route::get('comments/post/{post_id}',          'CommentController@index');
        Route::post('comments/{comment_id}/update',    'CommentController@update');
        Route::delete('comments/{comment_id}/destroy', 'CommentController@destroy');

        Route::post('comments/{comment_id}/like',      'LikeCommentController@toggleLike');
        Route::get('comments/{comment_id}/likes',      'LikeCommentController@likes');

        Route::get('user/{user_id}/followers',         'FollowController@followers');
        Route::get('user/{user_id}/followings',        'FollowController@followings');
        Route::get('user/{user_id}/mutual',            'FollowController@mutual');
        Route::post('follow/{user_id}',                'FollowController@follow');
        Route::post('unfollow/{user_id}',              'FollowController@unfollow');
        Route::post('followers/{user_id}/destroy',     'FollowController@destroy');

        Route::get('matriarch-minglin',                'MatriarchMinglinController@index');
    });
});

// FOR DEVELOPMENT MODE ONLY
Route::post('remove-users', function(Request $request) {
    $user_ids = explode(",", $request->users);

    App\User::whereIn('id', $user_ids)->forceDelete();

    return jsonResponse('success', 200, [
        'removed_users' => $user_ids
    ]);
});

Route::group(['middleware' => 'api'], function() {
    Route::get('test', function() {
        event(new App\Events\NewMessage('Test test'));
    });
});
